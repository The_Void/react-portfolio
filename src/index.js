import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
// Router
import { BrowserRouter } from 'react-router-dom';
// Redux
import { Provider } from 'react-redux';
import store from './redux/store.js';
// Bootstrap
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

const rootElement = document.getElementById('root');

ReactDOM.render(
    // Redux
    <Provider store={store}>
        // Router
        <BrowserRouter>
            // Main component
            <App /> 
        </BrowserRouter>
    </Provider>
    ,rootElement
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
